#Import the leaflet library for plotting maps in R
library(leaflet)

#If the above line of code returns an error as in: 
#  "Error in library(leaflet):there is no package called 'leaflet'"
#Then run the follow command to install the package:
install.packages("leaflet")

#Important! After installing, go back to the top and run `library(leaflet)` to load the library

#New York GPS
nyc_Latitude: 33.60
nyc_Longitude: 14.99

#Create a map
m <- leaflet() %>% setView(lng = nyc_Latitude, 
                           lat = nyc_Longitude, 
                           zoom = 12)
m %>% addTiles()
